//Written by Helen Fan

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <strsafe.h>
#include <cuda_profiler_api.h>
#include "hf_siddon_recon.hpp"

//maybe I should include how long it took to run the program in the parameter file???
//call it .info file instead of .cfg
//.info - generated using siddon_recon class, saves the information that was used to create files
//.cfg - used as input for the siddon_recon class for recon/projection/backprojection

int main()
{
	cudaProfilerStart();

	double totalTime;
	StopWatchInterface *timer;
	sdkCreateTimer(&timer);
	sdkResetTimer(&timer);
	sdkStartTimer(&timer);
	
	//CTSystem mysystem;
	//mysystem.a_SetCTSystemValue("CTParameters.cfg");
	//std::cout << "DeltaAng = " << mysystem.a_ScanParam.DeltaAng << std::endl;

	//---------------- how to use siddon_recon class --------------------------------
	siddon_recon recon;
	//recon.a1_FORWARD_PROJECTION("H:\\Dissertation\\Outline\\Images\\CT simulation\\rod_256-3\\360\\", "fp360_rod_256-3.info");
	recon.a0_RECON_MLEM("E:\\Images\\D700 images\\042515\\BreastPhantom Binary\\12 angles\\", "parameter.txt");
	//recon.a1_BACKWARD_PROJECTION("H:\\Visual Studio 2010\\CT_Recon\\CT_Recon", "MasterParameterFile.cfg", true);
	//-------------------------------------------------------------------
	

	//retrieving the sensitivity pointer
	//float *sensitivity = new float[recon.b_N_object_voxels];
	//CUDA_SAFE_CALL( cudaMemcpy(sensitivity, recon.a_dsensitivity_pointer(), recon.b_N_object_voxels*sizeof(float), cudaMemcpyDeviceToHost) );
	//SaveRawData<float> savefloat;
	//savefloat(sensitivity, recon.b_N_object_voxels*sizeof(float), recon.a_filepath.ReconFileFolder + "sensitivity.bin");
	//delete [] sensitivity;

	sdkStopTimer(&timer);
	totalTime = sdkGetTimerValue(&timer)*1e-3;
	printf("calculation time = %f seconds \n", totalTime);

	cudaProfilerStop();


	system("PAUSE");
}

